'use strict';

const util = require('util');
const express = require('express');
const router = express.Router();
const path = require('path');
const notes = require(process.env.NOTES_MODEL ?
  path.join('..', process.env.NOTES_MODEL) :
  '../models/notes-memory'
);
const log = require('debug')('notes:router-notes');
const error = require('debug')('notes:error');
const {
  ensureAuthenticated
} = require('./users');


router.get('/add', ensureAuthenticated, (req, res, next) => {
  res.render('noteedit', {
    title: "Add a Note",
    docreate: true,
    note: undefined,
    notekey: "",
    hideAddNote: true,
    user: req.user ? req.user : undefined,
    breadcrumbs: [{
        href: '/',
        text: 'Home'
      },
      {
        active: true,
        text: 'Add Note'
      },
    ],
  });
});

router.get('/edit', (req, res, next) => {
  notes.read(req.query.key)
    .then(note => {
      res.render('noteedit', {
        docreate: false,
        title: note ? `Edit ${note.title}` : '',
        notekey: req.query.key,
        note,
        user: req.user ? req.user : undefined,
        hideAddNote: true,
        breadcrumbs: [{
            href: '/',
            text: 'Home'
          },
          {
            active: true,
            text: note.title
          },
        ],
      })
    })
    .catch(err => {
      next(err);
    });
});

router.get('/view', (req, res, next) => {
  notes.read(req.query.key)
    .then(note => {
      res.render('noteview', {
        title: note ? note.title : '',
        notekey: req.query.key,
        note,
        hideAddNote: true,
        user: req.user ? req.user : undefined,
        breadcrumbs: [{
            href: '/',
            text: 'Home'
          },
          {
            active: true,
            text: note.title
          },
        ],
      })
    })
    .catch(err => {
      next(err);
    });
});

router.get('/destroy', (req, res, next) => {
  notes.read(req.query.key)
    .then(note => {
      res.render('notedestroy', {
        title: note ? note.title : "",
        notekey: req.query.key,
        note,
        user: req.user ? req.user : undefined,
        breadcrumbs: [{
            href: '/',
            text: 'Home'
          },
          {
            active: true,
            text: 'Delete Note'
          },
        ],
      })
    })
    .catch(err => {
      next(err);
    });
});

router.post('/destroy/confirm', ensureAuthenticated, (req, res, next) => {
  notes.destroy(req.body.notekey)
    .then(() => {
      res.redirect('/');
    })
    .catch(err => {
      next(err);
    });
});

// Save a note
router.post('/save', (req, res, next) => {
  var p;
  if (req.body.docreate === 'create') {
    p = notes.create(req.body.notekey, req.body.title, req.body.body);
  } else {
    p = notes.update(req.body.notekey, req.body.title, req.body.body);
  }
  p.then(note => {
      res.redirect(`/notes/view?key=${req.body.notekey}`);
    })
    .catch(err => {
      next(err);
    });
});

module.exports = router;
