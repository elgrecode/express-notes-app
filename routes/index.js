const express = require('express');
const router = express.Router();
const path = require('path');
const notes = require(process.env.NOTES_MODEL ?
  path.join('..', process.env.NOTES_MODEL) :
  '../models/notes-memory'
);
const util = require('util');

/* GET home page. */
router.get('/', function(req, res, next) {
  notes.keylist()
    .then(keylist => {
      var keyPromises = keylist.map(key => {
        return notes.read(key).then(note => {
          return {
            key: note.key,
            title: note.title
          }
        });
      });
      return Promise.all(keyPromises);
    })
    .then(notelist => {
      res.render('index', {
        title: 'Notes',
        notelist: notelist,
        user: req.user ? req.user : undefined,
        breadcrumbs: [{
          href: '/',
          text: 'Home'
        }]
      });
    })
    .catch(err => {
      next(err);
    });
});

module.exports = router;
